<?php
	session_start();
	
	function mostraAlerta($tipo) {
		if(isset($_SESSION[$tipo])) {
	?>
		<div class="text-center alert alert-<?=$tipo?> alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p><?=$_SESSION[$tipo]?></p>
		</div>				
	<?php
			unset($_SESSION[$tipo]);
		}
	}