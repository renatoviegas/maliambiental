<?php
    session_start();
    //-- Campos
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $telefone = $_POST['telefone'];
    $empresa = $_POST['empresa'];
    $mensagem = $_POST['mensagem'];
    $data_envio = date('d/m/Y');
    $hora_envio = date('H:i:s');
    
	//-- Corpo do E-mail   
	$html = "
		<!DOCTYPE html>
		<html lang='pt-br'> 
			<head>
				<meta charset='utf-8'/>
				<style type='text/css'>
					body {font-family: tahoma, arial, sans-serif; color:#000; margin: 0px; padding: 0px;} 
			
					table {
						border-collapse: collapse;
						padding: 0px;
						width: 700px;
						margin: 0px;                          
					}
					
					p {
						font-size: 9pt; 
						text-align: justify;
					}
				</style>
			</head>

			<body>
				<p>Este e-mail foi enviado em <b>{$data_envio}</b> às <b>{$hora_envio}</b></p>
				<table>
					<tr>
						 <td>Nome: {$nome}</td>
					</tr>
					
					<tr>
						<td>Empresa: {$empresa}</td>
					</tr>

					<tr>
						<td>E-mail: <b>{$email}</b></td>
					</tr>
					
					<tr>
						<td>Telefone: <b>{$telefone}</b> </td>
					</tr>
					
					<tr>
						<td>Mensagem: {$mensagem}</td>
					</tr>
				</table>
			</body>

		</html>";
	
    // email para quem será enviado o formulário
    $emailenviar = "marco.pinto@maliambiental.com.br";
    $destino = $emailenviar;
    $assunto = "Contato pelo Site - Mali";
 
    // É necessário indicar que o formato do e-mail é html
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= "From: Contato-Site<{$nome}>";
     
	error_reporting(0);
    $enviaremail = mail($destino, $assunto, $html, $headers);
	
    if($enviaremail){
		$_SESSION["success"] = "E-mail enviado com sucesso!";
    } else {
		$_SESSION["danger"] = "Erro ao tentar enviar o e-mail de contato!";		
    }
	
	header('Location: ../#contactUs');
	die();