<?php 
	error_reporting(E_ALL ^ E_NOTICE);
	require_once("php/mostra-alerta.php");
?>

<!DOCTYPE html>
<html lang="pt-br" class="no-js">
    <head>
        <!-- CONFIGURACOES DA PAGINA -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>	
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="google-site-verification" content="wsKhwTt9a3lc-cg9S_k3CN66K4PSGRXMpT94rfFD_bU"/>

        <!-- TITULO E DEFINICOES -->
        <title>MALI Ambiental</title>	
        <meta name="description" content="Ambiental, Consultoria, Meio Ambiente, Prestação de Serviços">
        <link rel="icon" href="favicon.png" type="image/png">

        <!-- ESTILOS -->
        <link rel="stylesheet" href="css/bootstrap.min.css" />     
        <link rel="stylesheet" href="css/styles.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/ackstyle.css"/>
    </head>

    <body id="page-top">
        <!-- LOGO E MENU DE NAVEGAÇÃO -->
        <header class="header bgBlack">
            <div class="container-fluid">
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>


                        <div class="logo-top">
                            <a href="#page-top" class="navbar-brand logo scroll-link">
                                <img id="image-logo" src="images/logo-mali.png" alt="Logo - Mali Ambiental">
                                <b>&nbsp;Ambiental</b>
                            </a>
                        </div>
                    </div>
                    <!--/.navbar-header-->
                    <div id="main-nav" class="collapse navbar-collapse navbar-inverse">
                        <ul class="nav navbar-nav" id="mainNav">
                            <li class="active"><a href="#page-top" class="scroll-link">Inicial</a></li>
                            <li><a href="#aboutUs" class="scroll-link">Sobre</a></li>
                            <li><a href="#services" class="scroll-link">Serviços</a></li>
                            <li><a href="#destination" class="scroll-link">Destinação Final</a></li>
                            <li><a href="#legislacao" class="scroll-link">Legislação</a></li>
                            <li><a href="#contactUs" class="scroll-link">Contato</a></li> 
                        </ul>
                    </div>
                    <!--/.navbar-collapse-->
                </nav>
                <!--/.navbar-->
            </div>
            <!--/.container-->
        </header>
        <!-- FIM LOGO E MENU -->

        <!-- HOME -->
        <section id="home" class="goHome">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class=""></li>
                    <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                </ol>

                <div class="carousel-inner">

                    <div class="item">
                        <img alt="Floresta" src="images/floresta.jpg" class="fullscreen">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Práticas econômicas e sustentáveis</h1>
                            </div>
                        </div>

                        <div class="divFrase">
                            <h3>"A Preservação Ambiental e a Responsabilidade Social representam um compromisso com o futuro do nosso Planeta."</h3>
                            <h5>Marco Pinto (Diretor da MALI)</h5>
                        </div>		            		

                    </div>


                    <div class="item active">
                        <img alt="Oceano" src="images/oceano.jpg" class="fullscreen">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Responsabilidade Ambiental</h1>	
                            </div>						
                        </div>

                        <div class="divFrase">
                            <h3>"O que eu faço, é uma gota no meio de um oceano. Mas sem ela, o oceano será menor."</h3>
                            <h5>Madre Teresa de Calcutá</h5>
                        </div>		 		          	

                    </div>

                    <div class="item">
                        <img alt="Industria" src="images/industria.jpg" class="fullscreen">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Tecnologia e Segurança</h1>
                            </div>	          	
                        </div>

                        <div class="divFrase">
                            <h3>"Uma pessoa inteligente resolve um problema, um sábio o previne."</h3>
                            <h5>Albert Einstein</h5>
                        </div>			          	

                    </div>

                </div>

                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </section>
        <!-- FIM HOME -->

        <!-- EMPRESA -->
        <section id="aboutUs" class="page-section painel-sobre">
            <div class="container">
                <div class="heading">
                    <!-- Heading -->
                    <h2>Sobre a MALI</h2>								 
                    <p>A <strong>MALI Consultoria e Assessoria em Meio Ambiente Ltda</strong> é uma empresa constituída em 2002 e desde a sua criação, sob a razão social Canela de Ema em Meio Ambiente Ltda, se dedica aos assuntos relacionados as atividades industriais e suas correlações com o meio ambiente.</p>
                    <p>A empresa em todos estes anos utiliza-se da experiência dos seus gestores para difundir, treinar, capacitar e assessorar outras empresas e outros profissionais com relação à <b>Gestão Ambiental</b> mais adequada e ao aprimoramento contínuo para os seus processos.</p>
                    <p>A MALI busca soluções particulares para cada um dos clientes, com base nas características dos seus negócios, desde a confecção do <b>Plano Diretor de Gestão Ambiental</b>, passando pelo Processo de <b>Licenciamento Ambiental</b> e implementação de um <b>Programa de Gerenciamento de Resíduos</b>, associados aos respectivos <b>Treinamentos das equipes</b>, <b>Supervisão/Controle do Processo de Gestão</b> e incluindo até a <b>Destinação Final</b> segura e adequada dos resíduos.</p>
                </div>

                <div class="row">
                    <div class="col-md-5 col-sm-5">										
                        <h3><i class="fa fa-bullseye color"></i> &nbsp;Missão</h3>
                        <p>Atender ao mercado com soluções ambientais tecnicamente corretas e seguras, respeitando sempre, os interesses de seus clientes, colaboradores e da comunidade em geral.</p>		
                    </div>

                    <div class="col-md-4 col-sm-4">										
                        <!-- Heading -->
                        <h3><i class="fa fa-eye color"></i> &nbsp;Visão</h3>
                        <!-- Paragraph -->
                        <p>Ser uma empresa parceira de referência no mercado nacional oferecendo soluções eficazes e seguras, e com os menores custos sociais e financeiros associados.</p>			
                    </div>

                    <div class="col-md-3 col-sm-3">										
                        <!-- Heading -->
                        <h3><i class="fa fa-diamond color"></i> &nbsp;Valores</h3>
                        <!-- Paragraph -->
                        <ul class="valores">
                            <li>Ética</li>
                            <li>Honestidade</li>
                            <li>Respeito</li>
                            <li>Responsabilidade</li>
                            <li>Qualidade</li>
                        </ul>				
                    </div>
                </div>
            </div>

            <div class="divFrase">
                <h3>"Ambiente limpo não é o que mais se limpa e sim o que menos se suja."</h3>
                <h5>Chico Xavier</h5>
            </div>
            <!--/.container-->
        </section>

        <!-- SERVIÇOS -->
        <section id="services" class="page-section painel-servico">
            <div class="container">
                <div class="heading text-center">
                    <!-- Heading -->
                    <h2>Serviços</h2>								
                </div>

                <div class="row">
                    <!-- item -->
                    <div class="col-md-4 text-center">                    
                        <i class="fa fa-bar-chart fa-2x circle"></i>
                        <h3><span class="id-color">Plano Diretor de Gestão</span></h3>
                        <p>Através do Levantamento da Legislação Ambiental existente relacionada à atividade do cliente, desenvolver um Plano de Gestão contemplando todas as atividades correlatas.</p>
                    </div><!-- end: -->

                    <!-- item -->
                    <div class="col-md-4 text-center"> 
                        <i class="fa fa-trash-o fa-2x circle"></i> 
                        <h3><span class="id-color">Gerenciamento de Resíduos</span></h3>
                        <p>O Gerenciamento de resíduos desde de 2010 passou a ser uma obrigação legar por parte das empresas através da Política Nacional de Resíduos Sólidos (PNRS) LEI no. 12.305/10.</p>
                    </div><!-- end: -->

                    <!-- item -->
                    <div class="col-md-4 text-center"> 
                        <i class="fa fa-file-text-o fa-2x circle"></i>                    
                        <h3><span class="id-color">Licenciamentos Ambientais</span></h3>
                        <p>Assessoria técnica para a obtenção de Licenças, Outorgas e Autorizações junto às autoridades públicas federais, estaduais e municipais.</p>
                    </div><!-- end: -->
                </div>

                <div class="row">

                    <!-- item -->
                    <div class="col-md-4 text-center"> 
                        <i class="fa fa-spoon fa-2x circle"></i>
                        <h3><span class="id-color">Coleta de Amostras</span></h3>
                        <p>Coleta de amostras conforme Norma NBR 10007.</p>          
                    </div><!-- end:-->

                    <!-- item -->
                    <div class="col-md-4 text-center"> 
                        <i class="fa fa-flask fa-2x circle"></i>
                        <h3><span class="id-color">Análises Laboratoriais</span></h3>
                        <p>Análise de caracterização de resíduos conforme Norma NBR 10004.</p>          
                    </div><!-- end:-->

                    <!-- item -->
                    <div class="col-md-4 text-center"> 
                        <i class="fa fa-truck fa-2x circle"></i>
                        <h3><span class="id-color">Gestão de Transporte de Resíduos</span></h3>
                        <p>Gestão Técnica da atividade de transporte de resíduos de acordo com as legislações pertinentes.</p>                     
                    </div><!-- end:-->
                </div>

                <div class="row">
                    <!-- item -->
                    <div class="col-md-4 text-center"> 
                        <i class="fa fa-tint fa-2x circle"></i> 
                        <h3><span class="id-color">Gestão de Resíduos de PCBs – ASCAREL (Bifenilas Policloradas)</span></h3>
                        <p>Elaboração de Plano de Trabalho, Rotograma, Licenciamento, Drenagem e Acondicionamento do óleo, Transporte do resíduo, Comboio especializado do veículo transportador e Destinação Final licenciada.</p>                    
                    </div><!-- end: -->

                    <!-- item -->
                    <div class="col-md-4 text-center"> 
                        <i class="fa fa-users fa-2x circle"></i>
                        <h3><span class="id-color">Terceirização de Mão de Obra</span></h3>
                        <h4><b>Cessão de mão obra capacitada em:</b></h4>
                        <ul class="lista">
                            <li>Manuseio de resíduos</li>
                            <li>Atuação na área de depósitos de resíduos</li>
                            <li>Operação de Aterros e ETE</li>
                        </ul>                      
                    </div><!-- end:-->

                    <!-- item -->
                    <div class="col-md-4 text-center"> 
                        <i class="fa fa-graduation-cap fa-2x circle"></i> 
                        <h3><span class="id-color">Treinamento de Equipes</span></h3>
                        <p>Treinamentos de equipes sobre Legislação Ambiental, Gerenciamento de Resíduos, Rotas de tratamento de resíduos, Análises laboratoriais, Tratamento de efluentes, transporte de resíduos, Compatibilidade Química, dentre outros assuntos.</p>  
                    </div><!-- end: -->				
                </div>
            </div>
            <!--/.container-->
			
			<div class="divFrase">
                <h3>"Ao contrário do que muitos pensam, não herdamos o planeta de nossos pais, e sim o tomamos emprestado de nossos filhos."</h3>
                <h5>Ditado Hindu</h5>
            </div>  			
        </section> 

        <!-- DESTINAÇÃO FINAL -->
        <section id="destination" class="page-section">

             <div class="container">
                <div class="heading text-center">
                    <!-- Heading -->
                    <h2>Destinação Final</h2>								
                </div>

                <div class="row">
                    <!-- item -->
                    <div class="col-md-3 text-center">                    
                        <i class="fa fa-recycle fa-2x circle"></i>
                        <h3><span class="id-color">Coleta Seletiva  e Valorização</span></h3>
                        <p>Coleta segregada de materiais valoráveis por tipologia e venda como material de insumo para outras atividades.</p>
                    </div><!-- end: -->

                    <!-- item -->
                    <div class="col-md-3 text-center"> 
                        <i class="fa fa-exchange fa-2x circle"></i>                    
                        <h3><span class="id-color">Logística Reversa</span></h3>
                        <p>A Legislação estabelece a prioridade de devolução aos fabricantes de resíduos, como lâmpadas fluorescentes, pilhas e baterias, óleos lubrificantes, embalagens de defensivos agrícolas, Lixo Tecnológico, dentre outros.</p>
                    </div><!-- end: -->

                    <!-- item -->
                    <div class="col-md-3 text-center"> 
                        <i class="fa fa-database fa-2x circle"></i> 
                        <h3><span class="id-color">Tratamento de Efluentes</span></h3>
                        <p>Tratamento físico-químico e biológico para águas oleosas e efluentes industriais.</p>
                    </div><!-- end: -->

                    <!-- item -->
                    <div class="col-md-3 text-center"> 
                        <i class="fa fa-battery-2 fa-2x circle"></i>
                        <h3><span class="id-color">Recuperação de Soluções Ácidas</span></h3>
                        <p>Processo Industrial que insere o resíduo novamente na cadeia produtiva. Através de reações químicas o resíduo entra como Matéria Prima para a produção de novos produtos.</p>          
                    </div><!-- end:-->

                    <!-- item -->
                    <div class="col-md-3 text-center"> 
                        <i class="fa fa-fire fa-2x circle"></i> 
                        <h3><span class="id-color">Descarte de Aerosóis</span></h3>
                        <p>Processo envolve a despressurização segura do recipiente, coleta e tratamento dos gases eliminados e a destinação final adequada do líquido drenado e coletado.</p>  
                    </div><!-- end: -->

                    <!-- item -->
                    <div class="col-md-3 text-center"> 
                        <i class="fa fa-building-o fa-2x circle"></i>
                        <h3><span class="id-color">Aterro Industrial</span></h3>
                        <p>Disposição de resíduos Classe I e Classe II A em células preparadas com tecnologias de engenharia que garantem a preservação das características do solo original e lençol freático.</p>                     
                    </div><!-- end:-->

                    <!-- item -->
                    <div class="col-md-3 text-center"> 
                        <i class="fa fa-cogs fa-2x circle"></i> 
                        <h3><span class="id-color">Coprocessamento de Resíduos</span></h3>
                        <p>Atividade de blendagem (mistura) de resíduos, de forma a preparar um resíduo padrão para ser incorporado no processo de fabricação de cimento, seja como substitutivo de Matéria Prima ou Combustível.</p>                    
                    </div><!-- end: -->

                    <!-- item -->
                    <div class="col-md-3 text-center"> 
                        <i class="fa fa-industry fa-2x circle"></i>
                        <h3><span class="id-color">Incineração</span></h3>
                        <p>Técnica de oxidação térmica de resíduos que ocorre em altas temperaturas (de 800 a 1200°C) sob um ambiente extremamente oxidante, sendo necessário o tratamento complementar dos gases gerados no processo de combustão.</p>             
                    </div><!-- end:-->

                </div>
                <br><br><br>
            </div>
            <!--/.container-->
        </section> 

        <!-- LEGISLAÇÃO -->
        <section id="legislacao" class="page-section section appear clearfix">	
            <div class="container">
                <div class="heading text-center">
                    <!-- Heading -->
                    <h2>Legislação</h2>								
                    <p>Seguem abaixo links contendo algumas Leis, Resoluções e Normas Técnicas pertinentes.</p>
                </div>

                <div>
                    <p><a href="http://www.planalto.gov.br/ccivil_03/LEIS/L6938.htm" target="_blank">Lei nº 6.938/81 - Política Nacional do Meio Ambiente</a></p>
                    <p><a href="http://www.planalto.gov.br/ccivil_03/_ato2007-2010/2010/lei/l12305.htm" target="_blank">Lei 12305/10 -  Política Nacional de Resíduos</a></p>
                    <p><a href="http://www.planalto.gov.br/ccivil_03/LEIS/L9605.htm" target="_blank">Lei nº 9.605/98 - LEI DE CRIMES AMBIENTAIS.</a></p>
                    <p><a href="files/PI.29.01.81.pdf"  target="_blank">Portaria 19/1981 - Uso, produção e comercialização e aquisição de equipamentos com PCBs.</a></p>
                    <p><a href="files/IN.10.06.83.pdf"  target="_blank">Instrução SEMA 001/83 - Armazenamento e transporte de bifenilas policloradas (PCBs).</a></p>
                    <p><a href="https://www.planalto.gov.br/ccivil_03/_Ato2004-2006/2005/Decreto/D5472.htm" target="_blank">Decreto No 5.472/05 – Promulga texto da Convenção de Estocolmo sobre POPs;</a></p>
                    <p><a href="http://www.legislacao.sp.gov.br/legislacao/index.htm" target="_blank">Lei 12.288/06 São Paulo - Destinação Final controlada dos resíduos de PCBs</a></p>
                    <p><a href="http://www.planalto.gov.br/ccivil_03/decreto/antigos/d96044.htm" target="_blank">Decreto 96044 – Transporte de Produtos Perigosos</a></p>
                    <p><a href="http://appasp.cnen.gov.br/seguranca/transporte/documentos/Resolucao-ANTT-420.pdf" target="_blank">Resolução 420 – Complementa Decreto 96044 – Transporte de Produtos Perigosos</a></p>
                    <p><a href="http://www.inmetro.gov.br/legislacao/rtac/pdf/RTAC001079.pdf" target="_blank">PORTARIA INMETRO 326/06 – Embalagens para Transporte Terrestre de Produtos Perigosos</a></p>
                    <p><a href="http://www.mma.gov.br/port/conama/legiabre.cfm?codlegi=262" target="_blank">CONAMA 264 – Coprocessamento de resíduos</a></p>
                    <p><a href="http://www.mma.gov.br/port/conama/legiabre.cfm?codlegi=335" target="_blank">CONAMA 313 - Inventário Nacional de Resíduos Sólidos Industriais.</a></p>
                    <p><a href="http://www.mma.gov.br/port/conama/legiabre.cfm?codlegi=338" target="_blank">CONAMA 316 – Sistema de tratamento térmico de resíduos</a></p>
                    <p><a href="http://www.mma.gov.br/port/conama/legiabre.cfm?codlegi=524" target="_blank">CONAMA 386 – Altera o artigo 18 da Resolução CONAMA 316/02</a></p>
                    <p><a href="http://www.mma.gov.br/port/conama/legiabre.cfm?codlegi=466" target="_blank">CONAMA 362 – óleos lubrificantes</a></p>
                    <p><a href="http://www.mma.gov.br/port/conama/legiabre.cfm?codlegi=674" target="_blank">CONAMA 450 – óleos lubrificantes</a></p>
                    <p><a href="http://www.mma.gov.br/port/conama/legiabre.cfm?codlegi=589" target="_blank">CONAMA 401 – Pilhas e baterias</a></p>
                    <p><a href="http://www.mma.gov.br/port/conama/legiabre.cfm?codlegi=629" target="_blank">CONAMA 424 – Pilhas e baterias</a></p>
                    <p><a href="http://www.mma.gov.br/port/conama/legiabre.cfm?codlegi=616" target="_blank">CONAMA 416 – Pneus inservíveis</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=943" target="_blank">NBR 8371 – Ascarel para transformadores e capacitores – características e riscos;</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=936" target="_blank">NBR 10004 – Resíduos sólidos - Classificação</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=1100" target="_blank">NBR 10005 - Procedimento para obtenção de extrato lixiviado de resíduos sólidos</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=1651" target="_blank">NBR 10006 - Procedimento para obtenção de extrato solubilizado de resíduos sólidos</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=1102" target="_blank">NBR 10007 - Amostragem em resíduos sólidos,</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=4278" target="_blank">NBR 10157 - Aterros de resíduos perigosos - Projeto, construção e operação - Procedimento</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=3298" target="_blank">NBR 12988 – Líquidos livres – Verificação em amostras de resíduos</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=2926" target="_blank">NBR 12235 – Armazenamento de resíduos perigosos</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=2926" target="_blank">NBR 13221 – Transporte terrestre de resíduos</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=4327" target="_blank">NBR13741 - Destinação de Bifenilas Policloradas;</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=337755" target="_blank">NBR 14619 – Transporte terrestre de produtos químicos – Incompatibilidade química</a></p>
                    <p><a href="http://www.abntcatalogo.com.br/norma.aspx?ID=322647" target="_blank">NBR 16725 – Ficha com dados de segurança de resíduos químicos e rotulagem</a></p>
                </div>
            </div>	
        </section>

        <!-- CONTATO -->
        <section id="contactUs" class="page-section">
            <div class="container">			
				
                <div class="row">
                    <div class="heading text-center">
                        <!-- Heading -->
                        <h2>Contato</h2> 
                        <p>Entre em contato conosco, será um prazer atendê-lo!</p>
                    </div> 
                </div>
				
				<?php 
					mostraAlerta("success");
					mostraAlerta("danger");
				?>

                <div class="row">
                    <div class="col-sm-4 text-center logo-rodape">
                        <img src="images/logo-mali.gif" alt="Logo - Mali Consultoria e Assessoria em Meio Ambiente">
                        <h2><span>MALI</span></h2>
                        <h2><span>Ambiental</span></h2>
                    </div>

                    <form action="php/enviarEmailContato.php" name="form_contato" method="post" role="form">
				
                        <div class="col-sm-4"> 
                            <div class="form-group">
                                <label for="nome">Nome *</label>
                                <input name="nome" type="text" class="form-control" placeholder="Informe o nome" title="Informe o seu nome" required>
                            </div>
                            <div class="form-group">
                                <label for="empresa">Empresa</label>
                                <input name="empresa" type="text" class="form-control" placeholder="Informe a empresa" title="Informe a empresa">
                            </div>
                            <div class="form-group">
                                <label for="email">Email *</label>
                                <input name="email" type="email" class="form-control" placeholder="Informe o e-mail" title="Entre com um e-mail válido" required>
                            </div>
                            <div class="form-group">
                                <label for="telefone">Telefone * (DDD + Número)</label>
                                <input name="telefone" class="form-control required digits" type="tel" size="30" value="" placeholder="Informe o telefone" title="Informe o telefone" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="mensagem">Mensagem *</label>
                                <textarea name="mensagem" class="form-control" cols="3" rows="9" placeholder="Entre com sua mensagem…" title="Entre com sua mensagem" required></textarea>
                            </div>	
                            <button name="submit" type="submit" class="btn btn-primary button-outline" id="submit">Enviar</button>		
                            <div class="result"></div>	
                        </div>                        									
                    </form>
                </div> 				
            </div>    
            <!--/.container-->
			
			<div class="divFrase divFraseContato">
				<h3>"Qualquer dano ao meio ambiente é um dano à humanidade."</h3>
				<h5>Papa Francisco</h5>
			</div> 
        </section> 

        <footer class="page-section painel-contato">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 about">
                        <h4>Redes Sociais</h4>
                        <p>Conecte-se a MALI nas redes sociais.</p>

                        <ul class="socialIcons">
                            <li><a href="https://www.facebook.com/maliambiental" class="fbIcon" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a></li>
                            <li><a href="#" class="twitterIcon" target="_blank"><i class="fa fa-twitter-square fa-lg"></i></a></li>
                            <li><a href="#" class="googleIcon" target="_blank"><i class="fa fa-google-plus-square fa-lg"></i></a></li>
                        </ul>
                    </div>

                    <div class="col-md-4 col-sm-6 twitter">
                        <!-- <h4>Latest Tweets</h4>
                        <ul>
                                <li><a href="#">@John Doe</a> Lorem ipsum dolor amet, consectetur adipiscing elit. Aenean leo lectus sollicitudin eget libero.</li>
                        </ul> -->
                    </div>

                    <div class="col-md-4 contact">
                        <h4>Contato</h4>
                        <ul>
                            <li><i class="fa fa-phone"></i>+55 (21) 3042-7450 / (21) 99908-3331</li>
                            <li>Fale com o diretor: <a href="#">marco.pinto@maliambiental.com.br <i class="fa fa-envelope-o"></i></a></li>
                        </ul>
                    </div>
                </div><!-- END Row -->
            </div>

        </footer>

        <!--/.page-section-->
        <section class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">          
                        <div class="pull-left copyRights">© Copyright 2016. Todos direitos reservados.</div>
                        <div class="pull-right copyRights">Desenvolvido por <a href="http://ackdata.com.br"> Ackdata Soluções Tecnologia</a></div>
                    </div>
                </div>  <!-- / .row -->
            </div>
        </section>

        <a href="#page-top" class="topHome scroll-link"><i class="fa fa-chevron-up fa-2x"></i></a>

        <!--[if lte IE 8]><script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script><![endif]-->
        <!-- ESTILOS -->
        <script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>    
        <script src="js/jquery.nav.js" type="text/javascript"></script>
        <script src="js/custom.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-76164307-1', 'auto');
		  ga('send', 'pageview');
		</script>
    </body>
</html>